package com.jbr570.animalapi.model;

public class Mammal extends Animal {
    public Mammal(String name) {
        super(name);
    }
    @Override
    public String toString() {
        return "Mammal[" + super.toString() + "]";
    }
}
