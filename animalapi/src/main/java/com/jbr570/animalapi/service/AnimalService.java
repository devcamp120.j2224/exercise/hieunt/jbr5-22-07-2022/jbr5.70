package com.jbr570.animalapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.jbr570.animalapi.model.*;

@Service
public class AnimalService {
    private static ArrayList<Animal> allAnimals = new ArrayList<>();
    static {
        Dog Dog1 = new Dog("Dog 1");
        Cat Cat1 = new Cat("Cat 1");
        Dog Dog2 = new Dog("Dog 2");
        Cat Cat2 = new Cat("Cat 2");
        Dog Dog3 = new Dog("Dog 3");
        Cat Cat3= new Cat("Cat 3");
        allAnimals.add(Dog1);
        allAnimals.add(Cat1);
        allAnimals.add(Dog2);
        allAnimals.add(Cat2);
        allAnimals.add(Dog3);
        allAnimals.add(Cat3);
    }
    public static ArrayList<Animal> getAllAnimals() {
        return allAnimals;
    }
}
