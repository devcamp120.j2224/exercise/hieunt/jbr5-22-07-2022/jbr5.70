package com.jbr570.animalapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr570.animalapi.service.AnimalService;
import com.jbr570.animalapi.model.*;


@RestController
@CrossOrigin
public class AnimalController {
    @GetMapping("/cats")
    public ArrayList<Animal> getAllCats() {
        ArrayList<Animal> allAnimals = AnimalService.getAllAnimals();
        ArrayList<Animal> result = new ArrayList<>();
        for (Animal bAnimal : allAnimals) {
            if(bAnimal instanceof Cat){
                result.add(bAnimal);
            }
        }
        return result;
    }

    @GetMapping("/dogs")
    public ArrayList<Animal> getAllDogs() {
        ArrayList<Animal> allAnimals = AnimalService.getAllAnimals();
        ArrayList<Animal> result = new ArrayList<>();
        for (Animal bAnimal : allAnimals) {
            if(bAnimal instanceof Dog){
                result.add(bAnimal);
            }
        }
        return result;
    }
}
